FROM openjdk:8-jdk-alpine

WORKDIR /usr/src/app 

COPY . .

RUN ./gradlew build

EXPOSE 8080

ENTRYPOINT [ "./gradlew", "bootRun" ]